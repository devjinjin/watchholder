package com.devjinjin.watchholder.view.add;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.devjinjin.watchholder.R;
import com.devjinjin.watchholder.connection.coinmarketcap.MarketCapItem;
import com.devjinjin.watchholder.view.add.AddFragment.OnListFragmentInteractionListener;
import com.devjinjin.watchholder.view.add.dummy.DummyContent.DummyItem;

import java.util.ArrayList;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MarketItemRecyclerViewAdapter extends RecyclerView.Adapter<MarketItemRecyclerViewAdapter.ViewHolder> {

    private final ArrayList<MarketCapItem> mValues;
    private final OnListFragmentInteractionListener mListener;
    private Context context;
    public MarketItemRecyclerViewAdapter(ArrayList<MarketCapItem> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(context)
                .inflate(R.layout.recycler_market_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mExchangeTextView.setText(mValues.get(position).getId());
        String url = "https://files.coinmarketcap.com/static/img/coins/64x64/"+mValues.get(position).getRank()+".png";
        Glide.with(context).load(url).into(holder.mIconImageView);

        holder.mNameTextView.setText(mValues.get(position).getName());
        holder.mRankTextView.setText(mValues.get(position).getRank());
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mValues != null) {
            return mValues.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        public final ImageView mIconImageView;
        public final TextView mNameTextView;
        public final TextView mMountTextView;
        public final TextView mRankTextView;
        public final TextView mPriceTextView;
        public final TextView mExchangeTextView;

        public final View view;
        public MarketCapItem mItem;

        public ViewHolder(View view) {
            super(view);
            this.view = view;

            this.mExchangeTextView = view.findViewById(R.id.tvExchange);
            this.mIconImageView = view.findViewById(R.id.ivHodlerIcon);
            this.mNameTextView = view.findViewById(R.id.tvName);
            this.mMountTextView = view.findViewById(R.id.tvMount);
            this.mPriceTextView = view.findViewById(R.id.tvPrice);
            this.mRankTextView = view.findViewById(R.id.tvRank);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mNameTextView.getText() + "'";
        }
    }
}
