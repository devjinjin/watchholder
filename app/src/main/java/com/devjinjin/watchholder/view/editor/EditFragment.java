package com.devjinjin.watchholder.view.editor;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.devjinjin.watchholder.BaseFragment;
import com.devjinjin.watchholder.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


/**
 * A fragment with a Google +1 button.
 * Activities that contain this fragment must implement the
 * {@link EditFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link EditFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EditFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    // The request code must be 0 or greater.
    private static final int PLUS_ONE_REQUEST_CODE = 0;
    // The URL to +1.  Must be a valid URL.
    private final String PLUS_ONE_URL = "http://developer.android.com";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    private OnFragmentInteractionListener mListener;

    @BindView(R.id.rcItemView)
    RecyclerView recyclerView;

    @BindView(R.id.tvText)
    TextView mTextView;

//    @BindView(R.id.btTest)
//    Button mButton;
//
//    @OnClick(R.id.btTest)
//    void onCallClick() {
//        mListener.onFragmentInteraction();
//    }


    //
//    MyRecyclerViewAdapter adapter;
    Unbinder unbinder;

    public EditFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EditFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EditFragment newInstance(String param1, String param2) {
        EditFragment fragment = new EditFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit, container, false);
        unbinder = ButterKnife.bind(this, view);

//        adapter = new MyRecyclerViewAdapter();
//        adapter.setOnUpdateItemListener(new MyRecyclerViewAdapter.OnUpdateItemListener() {
//            @Override
//            public void onLongPressItem(CoinInfo item) {
//                Toast.makeText(getContext(), "test : " + String.valueOf(item.getIndex()), Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onDeleteItem(CoinInfo item) {
//                mUpdateListener.onDeleteItem(item);
//                Toast.makeText(getContext(), "test : " + String.valueOf(item.getIndex()), Toast.LENGTH_SHORT).show();
//            }
//        });
//        LinearLayoutManager layoutManagaer = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
//        myCoinRecylerView.setLayoutManager(layoutManagaer);
//        myCoinRecylerView.setAdapter(adapter);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void setText(String text) {
        mTextView.setText(text);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
