package com.devjinjin.watchholder.connection.coinnest;

/**
 * ISL_KOREA
 * Created by jylee on 2018-02-28.
 */

public class Coinnest {
    private String[] items = new String[]{
            "btc", "bch", "btg", "bcd",
            "ubtc", "btn", "kst", "ltc",
            "act", "eth", "etc", "ada",
            "qtum", "xlm", "neo", "gas",
            "rpx", "qlc", "hsr", "knc", "tsl",
            "tron", "omg", "wtc", "mco", "storm",
            "gto", "pxs", "chat", "vet", "egcc",
            "ink", "oc", "hlc", "ent", "qbt",
            "spc", "put", "hotc"};

}
