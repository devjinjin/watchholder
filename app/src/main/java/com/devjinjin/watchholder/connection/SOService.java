package com.devjinjin.watchholder.connection;


import com.devjinjin.watchholder.connection.coinnest.CoinnestItem;
import com.devjinjin.watchholder.connection.coinmarketcap.MarketCapItem;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface SOService {
    @GET("/public/ticker/ALL")
    Call<CoinnestItem> getAnswers();

    @GET("/api/pub/ticker")
    Call<CoinnestItem> ticker(@Query(value = "coin", encoded = true) String tags);

//    @GET("/v1/ticker/?limit=10")
    @GET("/v1/ticker/")
    Call<ArrayList<MarketCapItem>> getMarketCapItem();
}
