package com.devjinjin.watchholder.connection;

public class ApiUtils {
    public static final String BASE_URL = "https://api.bithumb.com";
    public static final String C_BASE_URL = "https://api.coinnest.co.kr";
    public static final String COINMARKETCAP = "https://api.coinmarketcap.com";

    public enum REQUEST_TYPE{
        MARKET,
        BITHUMB,
        COINNEST
    }
    public static SOService getSOService(REQUEST_TYPE type) {
        switch (type){
            case MARKET:
                return RetrofitClient.getClient(COINMARKETCAP).create(SOService.class);
            case BITHUMB:
                return RetrofitClient.getClient(BASE_URL).create(SOService.class);
            case COINNEST:
                return RetrofitClient.getClient(C_BASE_URL).create(SOService.class);
        }

        return null;
    }
}
