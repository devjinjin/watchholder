package com.devjinjin.watchholder.connection.coinnest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class CoinnestItem {

    @SerializedName("high")
    @Expose
    private Integer high;
    @SerializedName("low")
    @Expose
    private Integer low;
    @SerializedName("buy")
    @Expose
    private Integer buy;
    @SerializedName("sell")
    @Expose
    private Integer sell;
    @SerializedName("last")
    @Expose
    private Integer last;
    @SerializedName("vol")
    @Expose
    private Double vol;
    @SerializedName("time")
    @Expose
    private Integer time;

    public Integer getHigh() {
        return high;
    }

    public void setHigh(Integer high) {
        this.high = high;
    }

    public Integer getLow() {
        return low;
    }

    public void setLow(Integer low) {
        this.low = low;
    }

    public Integer getBuy() {
        return buy;
    }

    public void setBuy(Integer buy) {
        this.buy = buy;
    }

    public Integer getSell() {
        return sell;
    }

    public void setSell(Integer sell) {
        this.sell = sell;
    }

    public Integer getLast() {
        return last;
    }

    public void setLast(Integer last) {
        this.last = last;
    }

    public Double getVol() {
        return vol;
    }

    public void setVol(Double vol) {
        this.vol = vol;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }


}
