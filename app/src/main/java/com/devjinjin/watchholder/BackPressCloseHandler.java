package com.devjinjin.watchholder;

import android.content.Context;
import android.widget.Toast;

public class BackPressCloseHandler {
    private long backKeyPressedTime = 0;
    private Toast mToast;
    private final IOnBackKeyPressListener mListener;
    private final Context mContext;

    public interface IOnBackKeyPressListener {
        void requestFinish();
    }

    public BackPressCloseHandler(Context pContext, IOnBackKeyPressListener pListener) {
        mListener = pListener;
        mContext = pContext;
    }

    public void onBackPressed() {
        if (System.currentTimeMillis() > backKeyPressedTime + 2000) {
            backKeyPressedTime = System.currentTimeMillis();
            showToast();
            return;
        }
        if (System.currentTimeMillis() <= backKeyPressedTime + 2000) {
            if (mToast != null) {
                mToast.cancel();
            }
            mListener.requestFinish();

        }
    }

    private void showToast() {
        mToast = Toast.makeText(mContext, R.string.press_back_key_again_finish_app, Toast.LENGTH_LONG);
        mToast.show();
    }
}

