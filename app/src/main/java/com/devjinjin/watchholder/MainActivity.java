package com.devjinjin.watchholder;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.devjinjin.watchholder.connection.ApiUtils;
import com.devjinjin.watchholder.connection.SOService;
import com.devjinjin.watchholder.connection.coinmarketcap.MarketCapItem;
import com.devjinjin.watchholder.connection.coinnest.CoinnestItem;
import com.devjinjin.watchholder.view.add.AddFragment;
import com.devjinjin.watchholder.view.editor.EditFragment;
import com.devjinjin.watchholder.view.viewer.ViewerFragment;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements EditFragment.OnFragmentInteractionListener, AddFragment.OnListFragmentInteractionListener {

    private BaseFragment fragment;
    private BackPressCloseHandler backPressCloseHandler;
    private ArrayList<MarketCapItem> currentDataList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        currentDataList = new ArrayList<>();
        if (savedInstanceState == null) {
            fragment = ViewerFragment.newInstance();
            replaceFragment(fragment, "VIEW");
        }
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                fragment = AddFragment.newInstance(1);
                addFragment(fragment, "EDIT");
                //                loadAnswers();
            }
        });

        backPressCloseHandler = new BackPressCloseHandler(this, new BackPressCloseHandler.IOnBackKeyPressListener() {
            @Override
            public void requestFinish() {
                onTerminateActivity();//Activity 종료
                android.os.Process.killProcess(android.os.Process.myPid());
            }
        });
    }

    private void onTerminateActivity() {
        moveTaskToBack(true);
        finish();
    }


    private void replaceFragment(Fragment fragment, String TAG) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment, fragment, TAG).commit();
    }

    private void addFragment(Fragment fragment, String TAG) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragment, fragment, TAG);//
        fragmentTransaction.addToBackStack(TAG);
        fragmentTransaction.commit();

    }

    @Override
    protected void onResume() {
        super.onResume();
        getSupportFragmentManager().addOnBackStackChangedListener(fragmentStackChangeListener);
        requestData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void loadAnswers() {

//        if(index >= items.length){
//            index = 0;
//        }
//        itemName = items[index];
//        index ++;
        SOService service = ApiUtils.getSOService(ApiUtils.REQUEST_TYPE.COINNEST);
        service.ticker("btc").enqueue(new Callback<CoinnestItem>() {
            @Override
            public void onResponse(Call<CoinnestItem> call, Response<CoinnestItem> response) {
                if (response.isSuccessful()) {

                    Log.d("MainActivity", "posts loaded from API");
                    CoinnestItem item = response.body();

                    fragment.setText(
                            "btc" + " : \n" +
                                    item.getBuy() + "\n" +
                                    item.getHigh() + "\n" +
                                    item.getLast() + "\n" +
                                    item.getLow() + "\n" +
                                    item.getSell() + "\n" +
                                    item.getTime() + "\n" +
                                    item.getVol());
                } else {
                    int statusCode = response.code();
                    // handle request errors depending on status code
                }
            }

            @Override
            public void onFailure(Call<CoinnestItem> call, Throwable t) {
//                showErrorMessage();
                Log.d("MainActivity", "error loading from API");

            }
        });
    }

    @Override
    public void onFragmentInteraction() {
        loadAnswers();
    }

    @Override
    public void onBackPressed() {
        if (fragment instanceof ViewerFragment) {
            backPressCloseHandler.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }

    private final FragmentManager.OnBackStackChangedListener fragmentStackChangeListener = new FragmentManager.OnBackStackChangedListener() {
        @Override
        public void onBackStackChanged() {
            int i = getSupportFragmentManager().getBackStackEntryCount();
            if (i == 0) {
                BaseFragment viewerfragment = (BaseFragment) getSupportFragmentManager().findFragmentByTag("VIEW");
                if (fragment != null) {
                    fragment = viewerfragment;
                }
            }
        }
    };

    @Override
    public void onListFragmentInteraction(MarketCapItem item) {

    }

    private void requestData() {
        SOService service = ApiUtils.getSOService(ApiUtils.REQUEST_TYPE.MARKET);
        service.getMarketCapItem().enqueue(new Callback<ArrayList<MarketCapItem>>() {
            @Override
            public void onResponse(Call<ArrayList<MarketCapItem>> call, Response<ArrayList<MarketCapItem>> response) {
                if (response.isSuccessful()) {
                    ArrayList<MarketCapItem> items = response.body();
                    Log.e("test", "test");
                    if(items!=null) {
                        currentDataList = items;
                        fragment.setMarketCapItemArray(items);
                    }
                } else {

                }
            }

            @Override
            public void onFailure(Call<ArrayList<MarketCapItem>> call, Throwable t) {

            }
        });
    }

    public ArrayList<MarketCapItem> getMarketCapItemArray() {
        return currentDataList;
    }
}
