package com.devjinjin.watchholder;

import android.support.v4.app.Fragment;

import com.devjinjin.watchholder.connection.coinmarketcap.MarketCapItem;

import java.util.ArrayList;

/**
 * ISL_KOREA
 * Created by jylee on 2018-02-28.
 */

public abstract class BaseFragment extends Fragment {
    protected ArrayList<MarketCapItem> marketCapItemArray;
    public abstract void setText(String text);

    public void setMarketCapItemArray(ArrayList<MarketCapItem> marketCapItemArray) {
        this.marketCapItemArray = marketCapItemArray;
    }
}
